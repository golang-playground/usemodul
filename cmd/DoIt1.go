package cmd

import (
	"fmt"

	"gitlab.com/golang-playground/testmodule"
	test3 "gitlab.com/golang-playground/testmodule/v3"
)

// Test1 print a string
func Test1() {
	fmt.Println("Test1 " + testmodule.GetString())
}

// Test3 print a string
func Test3() {
	fmt.Println("Test3 " + test3.GetString())
}
