package cmd

import (
	"fmt"

	"gitlab.com/golang-playground/testmodule/v2"
)

// Test2 prints simply a string
func Test2() {
	fmt.Println("Test2 " + testmodule.GetString())
}
