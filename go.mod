module gitlab.com/golang-playground/usemodul

go 1.15

require (
	gitlab.com/golang-playground/testmodule v0.0.0-20201108195156-e53fa819902a
	gitlab.com/golang-playground/testmodule/v2 v2.0.0-20201108200332-5d44dbf56922
	gitlab.com/golang-playground/testmodule/v3 v3.0.0-20201109065648-fd4629b66587
)
